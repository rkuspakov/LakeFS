
# Подготовка виртуального окружения

## Переходим в папку проекта
cd LakeFS

## Cоздаем виртуальное окружение
python -m venv venv

## Активируем виртуальное окружение
cd \venv\Scripts
venv\Scripts> .\activate

# Создание проекта

## Переходим в папку проекта
cd ..
cd ..

## Формирование данных lakectl.yaml
При установленном Docker запускаем команду на выполнение:
docker run --pull always --name lakefs -p 8000:8000 treeverse/lakefs:latest run --quickstart\
При этом у нас будут Access Key ID и Secret Access Key, которые понадобяться для авторизации по 
http://127.0.0.1:8000/repositories и хранящиеся в файле lakectl.yaml

## Запускаем команду (опционально, для первого знакомства)
docker run --name lakefs --rm --publish 8000:8000 treeverse/lakefs:latest run --local-settings

## Скачиваем дистрибутив и распаковываем в папку
\LakeFS\install

## Переходим в папку: \LakeFS\install и запускаем команду в новом терминальном окне:
    docker exec -it lakefs lakectl config
    и формируем файл: docker-compose.yaml, если он есть, то подтверждаем три параметра:
        access_key_id:
        secret_access_key:
        endpoint_url:

Полный файл можно скачать по ссылке https://github.com/treeverse/lakeFS-samples/blob/main/docker-compose.yml

## Монтируем папку проекта app/
В файле docker-compose.yaml добавляем секцию: 
volumes:
      - ./app:/home/lakefs/app

## Развертка контейнера
Для развертывания контейнера LakeFS необходимо запустить команду
docker-compose up

## Загрузка файлов
Для загрузки тестового файла в lakefs сначала надо зайти в контейнер командой:
docker exec -it -u 0 lakefs-example-lakefs-1 sh
затем установить make:
apk update && apk add make
Затем установить pipline командой:
cd app/ &&  make


Логирование:
(base) PS C:\Project\LakeFS\app> ***docker exec -it -u 0 lakefs-example-lakefs-1 sh***
/home/lakefs # ***apk update && apk add make***
fetch https://dl-cdn.alpinelinux.org/alpine/v3.18/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.18/community/x86_64/APKINDEX.tar.gz
v3.18.6-197-gcea87010f56 [https://dl-cdn.alpinelinux.org/alpine/v3.18/main]
v3.18.6-197-gcea87010f56 [https://dl-cdn.alpinelinux.org/alpine/v3.18/community]
OK: 20074 distinct packages available
(1/1) Installing make (4.4.1-r1)
Executing busybox-1.36.1-r5.trigger
OK: 8 MiB in 17 packages
/home/lakefs # ***cd app/ &&  make****
lakectl repo create lakefs://example-repo s3://example
Repository: lakefs://example-repo
Repository 'example-repo' created:
storage namespace: s3://example
default branch: main
timestamp: 1714852086
lakectl fs upload lakefs://example-repo/main/test.txt -s ./ver1.txt
Path: test.txt
Modified Time: 2024-05-04 19:48:07 +0000 UTC
Size: 14 bytes
Human Size: 14 B
Physical Address: s3://example/data/ggc759edc0p86bea8m9g/cor91tudc0p86bea8mag
Checksum: 25d9a43a1b627501c87010c302e6a245
Content-Type: application/octet-stream
lakectl commit lakefs://example-repo/main -m 'first version'
Branch: lakefs://example-repo/main
Commit for branch "main" completed.

ID: 52e37434dc3712bbf3481218cceb4e96412c46b6fd2cdb6d4566778e7ea2659f
Message: first version
Timestamp: 2024-05-04 19:48:07 +0000 UTC
Parents: 2dde43ceccf27af946abb6903fdb73c5f95aed21f2a5a207fad8c99d8dce34a3

lakectl fs upload lakefs://example-repo/main/test.txt -s ./ver2.txt
Path: test.txt
Modified Time: 2024-05-04 19:48:07 +0000 UTC
Size: 14 bytes
Human Size: 14 B
Physical Address: s3://example/data/ggc759edc0p86bea8m9g/cor91tudc0p86bea8mc0
Checksum: 111f48dad98df5893423a3c58d43b1b1
Content-Type: application/octet-stream
lakectl commit lakefs://example-repo/main -m 'second version'
Branch: lakefs://example-repo/main
Commit for branch "main" completed.

ID: 1829444ca50b9d717ba1d5d9d0d2b64e9e6080d2a5850a7747f24a3ba575b965
Message: second version
Timestamp: 2024-05-04 19:48:07 +0000 UTC
Parents: 52e37434dc3712bbf3481218cceb4e96412c46b6fd2cdb6d4566778e7ea2659f

lakectl fs download lakefs://example-repo/main/test.txt ./from_lakefs.txt
download: lakefs://example-repo/main/test.txt to /home/lakefs/app/from_lakefs.txt
echo 'All Done'
All Done